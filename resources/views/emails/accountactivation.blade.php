<!DOCTYPE html>
<html lang="es-VE">
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <h2>Verifica tu dirección de Correo Electrónico</h2>

        <div>
            Gracias por registrarte.

            Por favor sigue el enlace para verificar tu cuenta
            <a href="{{$app_url}}auth/register/verify/{{$user->confirmation_token}}">
            {{$app_url}}auth/register/verify/{{$user->confirmation_token}}</a><br/>
        </div>

    </body>
</html>