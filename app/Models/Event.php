<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Validator;

class Event extends Model
{

    use SoftDeletes;

    protected $dates = ['deleted_at'];
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'events';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
        'date_start',
        'date_finish',
        'organizer',
        'flyer_url',
        'category_id',
        'location_id'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    protected $appends = ['details'];

    /**
     * Relations
     */

    public function category()
    {
        return $this->belongsTo('App\Models\Category');
    }

    public function location()
    {
        return $this->belongsTo('App\Models\Location');
    }

    public function details()
    {
        return $this->hasMany('App\Models\EventDetail');
    }

    /**
     * Accessors
     */

    public function getDetailsAttribute()
    {
        return $this->details()->get();
    }
}

