<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::resource('authenticate', 'Auth\AuthController', ['only' => ['index', 'store']]);
// Password reset link request routes...
Route::post('password/email', 'Auth\PasswordController@postEmail');

// Password reset routes...
Route::post('password/reset', 'Auth\PasswordController@postReset');

Route::get('categories/{id}/events', [
  'as' => 'categories.events',
  'uses' => 'CategoriesController@events'
]);

Route::resource('categories/', 'CategoriesController',
  ['except' => ['create', 'edit']]);

Route::resource('locations/', 'LocationsController',
  ['except' => ['create', 'edit']]);

Route::resource('events/', 'EventsController',
  ['except' => ['create', 'edit']]);

Route::resource('eventDetails/', 'EventDetailsController',
  ['except' => ['create', 'edit']]);

Route::put('users/activate', [
  'as' => 'users.activate',
  'uses' => 'UsersController@activate'
]);

Route::get('users/resendactivation/{email}', [
  'as' => 'users.resendactivation',
  'uses' => 'UsersController@resendActivationMail'
]);

Route::resource('users/', 'UsersController',
  ['except' => ['create', 'edit']]);

Route::resource('roles/', 'RolesController');