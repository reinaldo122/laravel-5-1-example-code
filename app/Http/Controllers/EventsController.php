<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Request;

use App\Models\Event;

class EventsController extends Controller {

  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
    $this->middleware('jwt.auth',['except' => [
      'index',
      'show'
    ]]);
  }

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index()
  {
    return Event::all();
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function show($id)
  {
    return Event::findOrFail($id);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function update($id)
  {
    $event = Event::findOrFail($id);
    $event->name = Request::input('name');
    $event->description = Request::input('description');
    $event->date_start = Request::input('date_start');
    $event->date_finish = Request::input('date_finish');
    $event->organizer = Request::input('organizer');
    $event->flyer_url = Request::input('flyer_url');
    $event->category_id = Request::input('category_id');
    $event->location_id = Request::input('location_id');
    $event->save();
    return $event;
  }

  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */
  public function store()
  {
    //Create and set the values on the object
    $event = new Event(Request::only('name', 'description', 'date_start', 'date_finish', 'organizer', 'flyer_url', 'category_id', 'location_id'));
    $event->save();
    return response($event, 201);
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy($id)
  {
    try {
      $event = Event::findOrFail($id);
      if($event->destroy()) {
        return response('', 204);
      } else {
        return response(['status' => "Couldn't delete content"], 400);
      }
    } catch (Exception $e) {
      return response($e, 400);
    }
  }

}
