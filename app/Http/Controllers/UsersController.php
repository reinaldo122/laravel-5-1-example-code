<?php

namespace App\Http\Controllers;

use Request;
use Validator;
use Hash;
use Mail;
use JWTAuth;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\User;

class UsersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('jwt.auth',['except' => [
          'store', 'activate', 'resendActivationMail'
        ]]);

        $this->validation = [
            'email' => 'required|unique:users',
            'firstname' => 'required',
            'lastname' => 'required',
            'password' => 'required|min:8|confirmed',
            'birth_date' => 'date'
        ];

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $validator = Validator::make(Request::all(), $this->validation);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
        $user = new User(Request::all());
        $user->password = Hash::make(Request::input('password'));
        $user->confirmation_token = str_random(30);
        $user->save();
        Mail::send('emails.accountactivation', [
            'user' => $user
        ],function ($m) use ($user) {
            $m->from('no-reply@test.com.ve', 'Test Mail');
            $m->to($user->email, $user->firstname." ".$user->lastname)->subject("¡Activa tu cuenta!");
        });
        return response()->json($user,201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function activate()
    {
        if ($user = User::whereConfirmationToken(Request::input('confirmation_token'))->first()) {
            $user->confirmed = 1;
            $user->confirmation_token = null;
            $user->save();
            Mail::send('emails.accountactivated', [
                'user' => $user
            ],function ($m) use ($user) {
                $m->from('no-reply@test.com.ve', 'Avisado');
                $m->to($user->email, $user->firstname." ".$user->lastname)->subject("¡Cuenta activada!");
            });
            return response()->json(['activated' => true, 'access_token' => JWTAuth::fromUser($user)],202);
        }
        return response()->json(['error' => 'invalid_token'],404);
    }

    public function resendActivationMail($email)
    {
        if($user = User::whereEmail($email)->first()) {
            if ($user->confirmed == 0) {
                $user->confirmation_token = str_random(30);
                $user->save();
                Mail::send('emails.accountactivation', [
                    'user' => $user
                ],function ($m) use ($user) {
                    $m->from('no-reply@test.com.ve', 'Avisado');
                    $m->to($user->email, $user->firstname." ".$user->lastname)->subject("¡Activa tu cuenta!");
                });
                return response()->json(['activation_sent' => true],202);
            } else {
                return response()->json(['activation_sent' => false], 400);
            }
        } else {
            return response()->json(['activation_sent' => false], 404);
        }
    }
}
