<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Request;

use App\Models\Location;

class LocationsController extends Controller {

  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
    $this->middleware('jwt.auth',['except' => [
      'index',
      'show'
    ]]);
  }

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index()
  {
    return Location::all();
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function show($id)
  {
    $location = Location::findOrFail($id);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function update($id)
  {
    $location = Location::findOrFail($id);
    $location->name = Request::input('name');
    $location->address = Request::input('address');
    $location->address = Request::input('address');
    $location->latitude = Request::input('latitude');
    $location->longitude = Request::input('longitude');
    $location->save();
    return $location;
  }

  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */
  public function store()
  {
    //Create and set the values on the object
    $location = new Location(Request::only('name', 'address', 'latitude', 'longitude'));
    $location->save();
    return response($location, 201);
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy($id)
  {
    try {
      if(Location::destroy($id)) {
        return response('', 204);
      } else {
        return response(['status' => "Couldn't delete content"], 400);
      }
    } catch (Exception $e) {
      return response($e, 400);
    }
  }

}
