<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function ($faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->email,
        'password' => str_random(10),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Models\Category::class, function ($faker) {
    return [
        'name' => $faker->name,
        'desc' => $faker->text(120),
    ];
});
$factory->define(App\Models\Location::class, function ($faker) {
    return [
        'name' => $faker->streetName,
        'address' => $faker->address,
        'latitude' => $faker->latitude,
        'longitude' => $faker->longitude,
    ];
});

$factory->define(App\Models\Event::class, function ($faker) {
    return [
        'name' => $faker->name. ' festival.',
        'description' => $faker->text(500),
        'date_time' => $faker->dateTime,
        'organizer' => $faker->company,
        'cost' => $faker->randomFloat(2, 500, 10500),
        'flyer_url' => $faker->imageUrl(250,250),
        'category_id' => $faker->numberBetween(1,6),
        'location_id' => $faker->numberBetween(1,50),
    ];
});
