<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name', 80);
            $table->string('description', 500);
            $table->date('date_start');
            $table->date('date_finish');
            $table->string('organizer', 80);
            $table->string('flyer_url');
            //Foreign Key Columns
            $table->integer('category_id')->unsigned();
            $table->integer('location_id')->unsigned();
            //Foreign Key Relations
            $table->foreign('category_id')->references('id')->on('categories');
            $table->foreign('location_id')->references('id')->on('locations');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('events');
    }
}
