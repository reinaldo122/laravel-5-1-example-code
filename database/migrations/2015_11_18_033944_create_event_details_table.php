<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events_details', function(Blueprint $table){
            $table->increments('id');
            $table->dateTime('start');
            $table->dateTime('finish');
            $table->string('description', 200);
            $table->string('specific_location', 100);
            $table->float('cost');
            //Foreign Key Columns
            $table->integer('event_id')->unsigned();
            $table->integer('location_id')->unsigned();
            //Foreign Key Relations
            $table->foreign('event_id')->references('id')->on('events');
            $table->foreign('location_id')->references('id')->on('locations');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('events_details');
    }
}
