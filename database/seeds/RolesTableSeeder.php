<?php

use Illuminate\Database\Seeder;
use App\Models\Role;

class RolesTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    Role::create([
      'name' => 'admin',
      'display_name' => 'Avisado Admin',
      'description' => 'User is the sysadmin'
    ]);

    Role::create([
      'name' => 'staff',
      'display_name' => 'Avisado Staff',
      'description' => 'User is part of the Avisado Staff Team'
    ]);

    Role::create([
      'name' => 'manager',
      'display_name' => 'Manager',
      'description' => 'User can manage and publish as an Organization'
    ]);

    Role::create([
      'name' => 'user',
      'display_name' => 'User',
      'description' => 'A platform user'
    ]);
  }
}
